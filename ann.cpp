#include "ann.h"

ANN::ANN(const std::vector<size_t> &scheme,
         std::vector<KnownRule> rules,
         const std::tuple<double, double> &initWeightsRange,
         double errTolerance,
         size_t epochsLimit,
         double (*activationFunc)(double),
         double (*activationFuncDeriv)(double)) :
    m_weightsLen(0),
    m_rules(rules),
    m_errTolerance(errTolerance),
    m_epochsLimit(epochsLimit),
    m_fz(activationFunc),
    m_fzDeriv(activationFuncDeriv),
    m_epochsRun(0),
    m_err(0.0),
    m_state(New)
{
    for (size_t i = 1; i < scheme.size(); ++i)
    {
        Layer newLayer;
        std::vector<size_t> layerWOffs;
        auto layerInputsLen = scheme[i - 1] + 1;
        for (size_t j = 0; j < scheme[i]; ++j)
        {
            layerWOffs.push_back(m_weightsLen + j * layerInputsLen);
            newLayer.neurons.push_back(Neuron(scheme[i - 1], initWeightsRange));
        }
        m_weightsLen += scheme[i] * layerInputsLen;
        m_layers.push_back(newLayer);
        m_weightsOffs.push_back(layerWOffs);
    }
}

void ANN::setLearningRate(double learningRate)
{
    m_learningRate = learningRate;
}

void ANN::setMu(double mu)
{
    m_mu = mu;
}

void ANN::setNu(double nu)
{
    m_nu = nu;
}

ANN::EpochResult ANN::runTrainingEpoch(TrainingAlgorithm alg)
{
    if (m_state == Stopped) return EpochResult { Stopped, 0.0 };

    switch (alg)
    {
        case BP:
            runBPEpoch();
            break;
        case LMBP:
            runLMBPEpoch();
            break;
    }

    m_state = (++m_epochsRun == m_epochsLimit || m_err <= m_errTolerance) ? Stopped : Training;

    return EpochResult { m_state, m_err };
}

std::vector<double> ANN::evaluate(std::vector<double> inputs)
{
    std::vector<double> outputs;
    for (auto &layer : m_layers)
    {
        outputs.clear();
        for (auto &neuron : layer.neurons)
        {
            outputs.push_back(neuron.evaluate(inputs, m_fz));
        }
        inputs = outputs;
    }
    return outputs;
}

std::vector<double> ANN::layerOutputs(size_t idx) const
{
    std::vector<double> outs;
    for (const auto &neuron : m_layers[idx].neurons)
    {
        outs.push_back(neuron.out());
    }
    return outs;
}

std::vector<std::vector<std::vector<double>>> ANN::weights() const
{
    std::vector<std::vector<std::vector<double>>> w;
    for (const auto &layer : m_layers)
    {
        std::vector<std::vector<double>> layerWeights;
        for (const auto &neuron : layer.neurons)
        {
            layerWeights.push_back(neuron.weights());
        }
        w.push_back(layerWeights);
    }
    return w;
}

void ANN::runBPEpoch()
{
    m_err = 0.0;
    for (const auto &rule : m_rules)
    {
        evaluate(rule.inputs());
        calcSensitivities(rule);
        adjustWeights(rule);
    }
    m_err /= 2.0;
}

void ANN::calcSensitivities(const KnownRule &rule)
{
    // Last layer
    for (size_t i = 0; i < m_layers.back().neurons.size(); ++i)
    {
        auto &neuron = m_layers.back().neurons[i];
        double err = rule.outputs()[i] - neuron.out();
        m_err += (err * err);
        neuron.setSensitivity(-2.0 * m_fzDeriv(neuron.net()) * err);
    }
    // Remaining layers
    for (int m = static_cast<int>(m_layers.size()) - 2; m >= 0; --m)
    {
        auto &neurons = m_layers[static_cast<size_t>(m)].neurons;
        const auto &nextNeurons = m_layers[static_cast<size_t>(m + 1)].neurons;
        for (size_t i = 0; i < neurons.size(); ++i)
        {
            double sense = 0.0;
            double netValDeriv = m_fzDeriv(neurons[i].net());
            for (size_t k = 0; k < nextNeurons.size(); ++k)
            {
                const auto &next = nextNeurons[k];
                sense += netValDeriv * next.weights()[i + 1] * next.sensitivity();
            }
            neurons[i].setSensitivity(sense);
        }
    }
}

void ANN::adjustWeights(const KnownRule &rule)
{
    auto inputs = rule.inputs();
    for (auto &layer : m_layers)
    {
        std::vector<double> layerOutputs;
        inputs.insert(inputs.begin(), -1.0);
        for (auto &neuron : layer.neurons)
        {
            layerOutputs.push_back(neuron.out());
            for (size_t w = 0; w < neuron.weights().size(); ++w)
            {
                neuron.adjustWeight(w, -1 * m_learningRate * neuron.sensitivity() * inputs[w]);
            }
        }
        inputs = layerOutputs;
    }
}

void ANN::runLMBPEpoch()
{
    auto initW = flattenedWeights();
    auto x = 0;
    double err;
    do
    {
        Matrix w = initW;
        err = 0.0;
        for (const auto &rule : m_rules)
        {
            evaluate(rule.inputs());
            err += evaluateLMBPWeights(w, rule);
            calcLMBPJacobian(rule);
            w -= LMBPWeightsDelta();
            restoreLMBPWeights(w, m_layers);
        }
        err /= 2.0;
        if (err <= m_err)
        {
            m_mu /= m_nu;
            break;
        }
        m_mu *= m_nu;
        restoreLMBPWeights(initW, m_layers);
    }
    while (x++ < 6);
    m_err = err;
}

ANN::Matrix ANN::flattenedWeights()
{
    Matrix weights(m_weightsLen, 1);
    size_t i = 0;
    for (const auto &layer : m_layers)
    {
        for (const auto &neuron : layer.neurons)
        {
            for (auto w : neuron.weights())
            {
                weights(i++, 0) = w;
            }
        }
    }
    return weights;
}

double ANN::evaluateLMBPWeights(const Matrix &w, const KnownRule &rule)
{
    auto inputs = rule.inputs();
    auto copiedNetwork = m_layers;
    restoreLMBPWeights(w, copiedNetwork);
    std::vector<double> outputs;
    for (auto &layer : copiedNetwork)
    {
        outputs.clear();
        for (auto &neuron : layer.neurons)
        {
            outputs.push_back(neuron.evaluate(inputs, m_fz));
        }
        inputs = outputs;
    }
    double squaredErr = 0.0;
    m_errMatrix = Matrix(outputs.size(), 1);
    for (size_t i = 0; i < rule.outputs().size(); ++i)
    {
        auto err = rule.outputs()[i] - outputs[i];
        m_errMatrix(i, 0) = err;
        squaredErr += (err * err);
    }
    return squaredErr;
}

void ANN::calcLMBPJacobian(const KnownRule &rule)
{
    // a^l_0 = -1.0 -> Input to next layer biases
    // Jacobian Matrix de/dw
    m_jac = Matrix(m_layers.back().neurons.size(), m_weightsLen, 0.0);
    // LMBP Sensitivities
    std::vector<std::vector<std::vector<double>>> senses;
    for (const auto &layer : m_layers)
    {
        senses.push_back(std::vector<std::vector<double>>(
            layer.neurons.size(), std::vector<double>(m_layers.back().neurons.size(), 0.0)
        ));
    }
    // Last layer
    for (size_t i = 0; i < m_layers.back().neurons.size(); ++i)
    {
        auto &neuron = m_layers.back().neurons[i];
        auto netDeriv = m_fzDeriv(neuron.net());
        for (size_t j = 0; j < neuron.weights().size(); ++j)
        {
            auto wInp = j == 0 ? -1.0 : m_layers[m_layers.size() - 2].neurons[j - 1].out();
            m_jac(i, m_weightsOffs.back()[i] + j) = -netDeriv * wInp;
        }
        senses.back()[i][i] = -netDeriv;
    }
    // Remaining layers
    for (int m = static_cast<int>(m_layers.size()) - 2; m >= 0; --m)
    {
        auto mm = static_cast<size_t>(m);
        auto &neurons = m_layers[mm].neurons;
        const auto &nextNeurons = m_layers[mm + 1].neurons;
        // Iter over neurons
        for (size_t i = 0; i < neurons.size(); ++i)
        {
            const auto &neuron = neurons[i];
            auto netDeriv = m_fzDeriv(neuron.net());
            // Iter over errors
            for (size_t e = 0; e < m_layers.back().neurons.size(); ++e)
            {
                double sense = 0.0;
                // Iter over next layer neurons
                for (size_t k = 0; k < nextNeurons.size(); ++k)
                {
                    const auto &next = nextNeurons[k];
                    sense += senses[mm + 1][k][e] * next.weights()[i + 1] * netDeriv;
                }
                // Iter over weights
                for (size_t j = 0; j < neurons[i].weights().size(); ++j)
                {
                    auto wInp = j == 0 ? -1.0 :
                                m == 0 ? rule.inputs()[j - 1]
                                       : m_layers[mm - 1].neurons[j - 1].out();
                    m_jac(e, m_weightsOffs[mm][i] + j) = sense * wInp;
                }
                senses[mm][i][e] = sense;
            }
        }
    }
}

ANN::Matrix ANN::LMBPWeightsDelta()
{
    Matrix muIdent = m_mu * blaze::IdentityMatrix<double>(m_jac.columns());
    Matrix transJac = blaze::trans(m_jac);
    Matrix inverted = blaze::inv((transJac * m_jac) + muIdent);
    return inverted * transJac * m_errMatrix;
}

void ANN::restoreLMBPWeights(const Matrix &w, std::vector<Layer> &network)
{
    size_t x = 0;
    for (size_t m = 0; m < network.size(); ++m)
    {
        for (size_t i = 0; i < network[m].neurons.size(); ++i)
        {
            for (size_t j = 0; j < network[m].neurons[i].weights().size(); ++j)
            {
                network[m].neurons[i].setWeight(j, w(x++, 0));
            }
        }
    }
}

const std::vector<KnownRule> &ANN::rules() const
{
    return m_rules;
}

double ANN::learningRate() const
{
    return m_learningRate;
}

double ANN::mu() const
{
    return m_mu;
}

double ANN::nu() const
{
    return m_nu;
}

double ANN::errTolerance() const
{
    return m_errTolerance;
}

size_t ANN::epochsLimit() const
{
    return m_epochsLimit;
}

size_t ANN::epochsRun() const
{
    return m_epochsRun;
}

double ANN::err() const
{
    return m_err;
}

ANN::State ANN::state() const
{
    return m_state;
}
