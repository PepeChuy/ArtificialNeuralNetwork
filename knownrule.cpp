#include "knownrule.h"

KnownRule::KnownRule(std::vector<double> inputs, std::vector<double> outputs):
    in(inputs), out(outputs) {}

const std::vector<double> &KnownRule::inputs() const { return in; }

const std::vector<double> &KnownRule::outputs() const { return out; }
