#ifndef ANN_H
#define ANN_H

#include "neuron.h"
#include "knownrule.h"
#include "blaze/math/DynamicMatrix.h"

#include <vector>

class ANN
{
    public:

        enum State
        {
            New,
            Training,
            Stopped
        };

        enum TrainingAlgorithm
        {
            BP,     // Backpropagation
            LMBP    // Levenberg-Marquardt Backpropagation
        };

        struct EpochResult
        {
            State state;
            double err;
        };

        ANN(const std::vector<size_t> &scheme,
            std::vector<KnownRule> rules,
            const std::tuple<double, double> &initWeightsRange,
            double errTolerance,
            size_t epochsLimit,
            double (*activationFunc)(double),
            double (*activationFuncDeriv)(double));

        void setLearningRate(double learningRate);
        void setMu(double mu);
        void setNu(double nu);

        EpochResult runTrainingEpoch(TrainingAlgorithm alg);
        std::vector<double> evaluate(std::vector<double> inputs);

        std::vector<double> layerOutputs(size_t idx) const;
        std::vector<std::vector<std::vector<double>>> weights() const;

        const std::vector<KnownRule> &rules() const;
        double learningRate() const;
        double mu() const;
        double nu() const;
        double errTolerance() const;
        size_t epochsLimit() const;
        size_t epochsRun() const;
        double err() const;
        State state() const;

    private:

        using Matrix = blaze::DynamicMatrix<double>;

        struct Layer
        {
            std::vector<Neuron> neurons;
        };

        // Backpropagation
        void runBPEpoch();
        void calcSensitivities(const KnownRule &rule);
        void adjustWeights(const KnownRule &rule);

        // Levenberg-Marquardt Backpropagation
        void runLMBPEpoch();
        Matrix flattenedWeights();
        double evaluateLMBPWeights(const Matrix &w, const KnownRule &rule);
        void calcLMBPJacobian(const KnownRule &rule);
        Matrix LMBPWeightsDelta();
        void restoreLMBPWeights(const Matrix &w, std::vector<Layer> &network);
        size_t m_weightsLen;
        std::vector<std::vector<size_t>> m_weightsOffs;
        Matrix m_jac;
        Matrix m_errMatrix;

        std::vector<Layer> m_layers;
        std::vector<KnownRule> m_rules;
        double m_learningRate;
        double m_mu;
        double m_nu;
        double m_errTolerance;
        size_t m_epochsLimit;
        double (*m_fz)(double);
        double (*m_fzDeriv)(double);
        size_t m_epochsRun;
        double m_err;
        State m_state;
};

#endif // ANN_H
