#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ann.h"
#include "qcustomplot.h"

#include <tuple>
#include <vector>

#include <QMainWindow>
#include <QMouseEvent>
#include <QTimer>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
        Q_OBJECT

    public:
        explicit MainWindow(QWidget *parent = nullptr);
        ~MainWindow();

    public slots:
        void handlePlotClick(QMouseEvent *ev);
        void handleTrainEpoch();
        void updateState();
        void logErr(size_t epoch, double err);
        void plotState(QCPGraph *graph, const std::vector<double> &w, double th);
        void classifySpace();
        void graphResults();
        void graphFirstLayer();

    private slots:
        void on_spinSearchRangeStart_valueChanged(double arg1);
        void on_spinSearchRangeEnd_valueChanged(double arg1);
        void on_btnInit_clicked();
        void on_btnTrain_clicked();
        void on_btnPause_clicked();
        void on_btnClear_clicked();
        void onPlotModeSelected();

        void on_addClassButton_clicked();

private:
        Ui::MainWindow *ui;
        QTimer clock;

        std::vector<KnownRule> rules;
        ANN *neuralNetwork;

        const int MAX_COLUMNS = 3;

        const std::tuple<double, double> xAxisRange = {-10.0, 10.0};
        const std::tuple<double, double> yAxisRange = {-10.0, 10.0};
        const double axisStep = 0.1;
        std::vector<QColor> colors;
        std::vector<QCPGraph *> classGraphs;
        std::vector<QCPGraph *> firstLayerGraphs;
        std::vector<QCPColorMap*> gradientGraphs;
        QCPColorGradient grad;
        QCPBars *errorGraph;

        QButtonGroup plotModeGroup;
        QButtonGroup classGroup;

        std::vector<double> classId2Vec(int classId, size_t classesN);
        std::pair<size_t, double> vec2ClassIdVal(std::vector<double> resultVector);
        static double logsig(double y) { return 1.0 / (1.0 + std::exp(-y)); }
        static double logsigDeriv(double y) { return logsig(y) * (1 - logsig(y)); }
};

#endif // MAINWINDOW_H
