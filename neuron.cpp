#include "neuron.h"

#include <random>       /* std::random_device, std::mt19937,
                           std::uniform_real_distribution */
#include <algorithm>    // std::generate

Neuron::Neuron(size_t inputsN, const std::tuple<double, double> &initWeightsRange) :
    m_weights(inputsN + 1),
    m_net(0)
{
    // Initialize state
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<double> randDistrib(
        std::get<0>(initWeightsRange), std::get<1>(initWeightsRange)
    );
    std::generate(m_weights.begin(), m_weights.end(),
        [&]() -> double { return randDistrib(gen); }
    );
}

void Neuron::setWeight(size_t idx, double w)
{
    m_weights[idx] = w;
}

void Neuron::adjustWeight(size_t idx, double delta)
{
    m_weights[idx] += delta;
}

void Neuron::setSensitivity(double sensitivity)
{
    m_sensitivity = sensitivity;
}

double Neuron::evaluate(std::vector<double> inputs, double (*fz)(double))
{
    m_net = 0.0;
    inputs.insert(inputs.begin(), -1.0);
    for (size_t i = 0; i < m_weights.size(); ++i)
    {
        m_net += m_weights[i] * inputs[i];
    }
    m_out = fz(m_net);
    return m_out;
}

const std::vector<double> &Neuron::weights() const
{
    return m_weights;
}

double Neuron::net() const
{
    return m_net;
}

double Neuron::out() const
{
    return m_out;
}

double Neuron::sensitivity() const
{
    return m_sensitivity;
}
