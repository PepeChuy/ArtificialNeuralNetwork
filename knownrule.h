#ifndef KNOWNRULE_H
#define KNOWNRULE_H

#include <vector>

class KnownRule
{
    public:
        KnownRule(std::vector<double> inputs, std::vector<double> outputs);
        const std::vector<double> &inputs() const;
        const std::vector<double> &outputs() const;
    private:
        std::vector<double> in;
        std::vector<double> out;
};

#endif // KNOWNRULE_H
