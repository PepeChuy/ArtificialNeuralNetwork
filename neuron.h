#ifndef NEURON_H
#define NEURON_H

#include <tuple>
#include <vector>

class Neuron
{
    public:
        Neuron(size_t inputsN, const std::tuple<double, double> &initWeightsRange);

        double evaluate(std::vector<double> inputs, double (*fz)(double));

        void setWeight(size_t idx, double w);
        void adjustWeight(size_t idx, double delta);
        void setSensitivity(double sensitivity);

        const std::vector<double> &weights() const;
        double net() const;
        double out() const;
        double sensitivity() const;
    private:
        std::vector<double> m_weights;
        double m_net;
        double m_out;
        double m_sensitivity;
};

#endif // NEURON_H
